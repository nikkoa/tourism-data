<?php

namespace App\Http\Controllers;

use App\BorderEntry;
use Illuminate\Http\Request;

class BorderEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BorderEntry  $borderEntry
     * @return \Illuminate\Http\Response
     */
    public function show(BorderEntry $borderEntry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BorderEntry  $borderEntry
     * @return \Illuminate\Http\Response
     */
    public function edit(BorderEntry $borderEntry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BorderEntry  $borderEntry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BorderEntry $borderEntry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BorderEntry  $borderEntry
     * @return \Illuminate\Http\Response
     */
    public function destroy(BorderEntry $borderEntry)
    {
        //
    }
}
