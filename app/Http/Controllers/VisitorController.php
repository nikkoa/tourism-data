<?php

namespace App\Http\Controllers;

use App\Visitor;
use Illuminate\Http\Request;

class VisitorController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$query = Visitor::query();
		$data = [];
		foreach (['name', 'date_of_birth', 'country_of_birth', 'current_nationality', 'gender', 'marital_status'] as $prop) {
			if ($request->has($prop)) {
				$data[$prop] = $request->$prop;
			}
		}
		if (sizeof($data)) {
			$query = $query->where($data);
		}
		return $query->paginate(10);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$input = $request->validate([
			'name'                => 'required|string',
			'date_of_birth'       => 'required|date',
			'passport_number'     => 'required|alpha_num|size:10',
			'country_of_birth'    => 'nullable|integer|exists:countries,id',
			'current_nationality' => 'nullable|integer|exists:countries,id',
			'gender'              => 'nullable|in:m,f,M,F',
			'marital_status'      => 'nullable|in:single,married,widowed,divorced,SINGLE,MARRIED,WIDOWED,DIVORCED',
		]);

		$faker = \Faker\Factory::create();
		$input['uuid'] = $faker->uuid;
		$visitor = Visitor::create($input);

		return $visitor;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Visitor  $visitor
	 * @return \Illuminate\Http\Response
	 */
	public function show(Visitor $visitor) {
		return $visitor;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Visitor  $visitor
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Visitor $visitor) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Visitor  $visitor
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Visitor $visitor) {
		$input = $request->validate([
			'name'                => 'required|string',
			'date_of_birth'       => 'required|date',
			'passport_number'     => 'required|alpha_num|size:10',
			'country_of_birth'    => 'nullable|integer|exists:countries,id',
			'current_nationality' => 'nullable|integer|exists:countries,id',
			'gender'              => 'nullable|in:m,f,M,F',
			'marital_status'      => 'nullable|in:single,married,widowed,divorced,SINGLE,MARRIED,WIDOWED,DIVORCED',
		]);

		$visitor->update($input);
		return $visitor;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Visitor  $visitor
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Visitor $visitor) {
		return $visitor->delete();
	}
}
