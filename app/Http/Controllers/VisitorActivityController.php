<?php

namespace App\Http\Controllers;

use App\VisitorActivity;
use Illuminate\Http\Request;

class VisitorActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VisitorActivity  $visitorActivity
     * @return \Illuminate\Http\Response
     */
    public function show(VisitorActivity $visitorActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VisitorActivity  $visitorActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(VisitorActivity $visitorActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VisitorActivity  $visitorActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VisitorActivity $visitorActivity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VisitorActivity  $visitorActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(VisitorActivity $visitorActivity)
    {
        //
    }
}
