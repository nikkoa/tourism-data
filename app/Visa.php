<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visa extends Model
{
	protected $primaryKey = 'uuid';
	public $incrementing = false;
    protected $dates = ['created_at', 'updated_at', 'issue_date', 'expiry_date'];
}
