<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorActivity extends Model
{
	protected $primaryKey = 'uuid';
	public $incrementing = false;
    protected $dates = ['created_at', 'updated_at', 'visit_date'];
}
