<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorderEntriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('border_entries', function (Blueprint $table) {
			$table->uuid('uuid');
			$table->string('passport_number');
			$table->string('visa_number');
			$table->string('port_of_entry');
			$table->string('origin');
			$table->string('flight_number');
			$table->date('entry_date');
			$table->boolean('is_flagged')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('border_entries');
	}
}
