<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('visitors', function (Blueprint $table) {
			$table->uuid('uuid');
			$table->string('name');
			$table->date('date_of_birth');
			$table->string('passport_number');
			$table->integer('country_of_birth')->nullable();
			$table->integer('current_nationality')->nullable();
			$table->enum('gender', ['M', 'F'])->nullable();
			$table->enum('marital_status', ['single', 'married', 'widowed', 'divorced'])->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('visitors');
	}
}
