<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisasTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('visas', function (Blueprint $table) {
			$table->uuid('uuid');
			$table->string('visa_number');
			$table->string('passport_number');
			$table->string('place_of_issue')->nullable();
			$table->enum('passport_type', ['diplomatic', 'official', 'ordinary']);
			$table->enum('visa_type', ['tourist', 'official', 'business', 'employment', 'student', 'other']);
			$table->date('issue_date');
			$table->date('expiry_date');
			$table->boolean('is_multiple')->default(false);
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('visas');
	}
}
