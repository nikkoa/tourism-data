<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorderExitsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('border_exits', function (Blueprint $table) {
			$table->uuid('uuid');
			$table->string('passport_number');
			$table->string('visa_number');
			$table->string('port_of_exit');
			$table->string('destination');
			$table->string('flight_number');
			$table->date('exit_date');
			$table->boolean('is_flagged')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('border_exits');
	}
}
