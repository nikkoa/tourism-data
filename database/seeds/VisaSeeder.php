<?php

use Illuminate\Database\Seeder;

class VisaSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();
		$passport_types = ['diplomatic', 'official', 'ordinary'];
		$visa_types = ['tourist', 'official', 'business', 'employment', 'student', 'other'];

		\App\Visitor::chunk(1000, function ($visitors) use ($faker, $passport_types, $visa_types) {
			foreach ($visitors as $visitor) {

				// dumb data
				$data = [
					'uuid'            => $faker->uuid,
					'visa_number'     => strtoupper($faker->bothify('??#######')),
					'visa_type'       => $faker->randomElement($visa_types),
					'passport_number' => $visitor->passport_number,
					'passport_type'   => $faker->randomElement($passport_types),
					'place_of_issue'  => $visitor->country_of_birth,
					'issue_date'      => $faker->date('Y-m-d', '-2 years'),
					'expiry_date'     => $faker->date('Y-m-d', '-2 years'),
					'is_multiple'     => $faker->randomElement([true, false]),
				];
				$visa = \App\Visa::create($data);

				// Modify the issue and expiry date based on visitor date of birth
				$age = rand(8, $visitor->date_of_birth->diffInYears(\Carbon\Carbon::parse('1 year ago')));
				$issue = $faker->dateTimeBetween($startDate = $visitor->date_of_birth->addYear($age), $endDate = '-1 year', $timezone = null);
				$expiry = $visa->issue_date->addYear($faker->randomElement([2, 5, 10]));

				$visa->update([
					'issue_date'  => $issue,
					'expiry_date' => $expiry,
				]);
			}
		});
	}
}
