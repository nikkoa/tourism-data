<?php

use Illuminate\Database\Seeder;

class BorderSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();
		// $port_of_entry = $faker->randomElement(['GLU', 'BUT', 'PBH', 'YON']);
		\App\Visa::chunk(1000, function ($visas) use ($faker) {
			foreach ($visas as $visa) {

				$port_of_entry = 'PBH';
				$origin = $faker->randomElement(['BKK', 'DEL', 'CCU', 'IXB', 'GAY', 'GAU', 'KTM', 'DAC', 'SIN']);

				$flights = [
					'BKK' => ['KB151', 'KB121', 'KB131'],
					'DEL' => ['KB205'],
					'CCU' => ['KB121', 'KB211'],
					'IXB' => ['KB131'],
					'GAY' => ['KB213', 'KB123'],
					'GAU' => ['KB141'],
					'KTM' => ['KB401', 'KB4001'],
					'DAC' => ['KB301'],
					'SIN' => ['KB501'],
				];

				$entry = [
					'uuid'            => $faker->uuid,
					'passport_number' => $visa->passport_number,
					'visa_number'     => $visa->visa_number,
					'port_of_entry'   => $port_of_entry,
					'origin'          => $origin,
					'flight_number'   => $faker->randomElement($flights[$origin]),
					'entry_date'      => $visa->issue_date,
					'is_flagged'      => false,
				];

				\App\BorderEntry::create($entry);

				$port_of_exit = 'PBH';
				$destination = $faker->randomElement(['BKK', 'DEL', 'CCU', 'IXB', 'GAY', 'GAU', 'KTM', 'DAC', 'SIN']);
				$flights = [
					'BKK' => ['KB150', 'KB120', 'KB130'],
					'DEL' => ['KB204'],
					'CCU' => ['KB120', 'KB210'],
					'IXB' => ['KB130'],
					'GAY' => ['KB212', 'KB122'],
					'GAU' => ['KB140'],
					'KTM' => ['KB400', 'KB4000'],
					'DAC' => ['KB300'],
					'SIN' => ['KB500'],
				];
				$exit = [
					'uuid'            => $faker->uuid,
					'passport_number' => $visa->passport_number,
					'visa_number'     => $visa->visa_number,
					'port_of_exit'    => $port_of_exit,
					'destination'     => $destination,
					'flight_number'   => $faker->randomElement($flights[$destination]),
					'exit_date'       => $visa->issue_date,
					'is_flagged'      => false,
				];

				\App\BorderExit::create($exit);
			}

		});
	}
}
