<?php

use Illuminate\Database\Seeder;

class PointOfInterestSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();
		$poi = [
			'Phuentsholing'       => ['Zangtho Pelri Lhakhang'],
			'Gangtey (Phobjikha)' => ['Gangtey Goempa', 'Black Necked Crane Information Centre', 'Kumbu Lhakhang'],
			'Trashiyangtse'       => ['Chorten Kora', 'Trashiyangtse Dzong', 'Bomdelling', 'Ranjung Woesel Chholing'],
			'Paro'                => ['Rinpung Dzong', 'Ta Dzong', 'Drukgyel Dzong', 'Kyichu Lhakhang', 'Druk Choeding', 'Dungtse Lhakhang', 'Ugyen Pelri Palace', 'Jangsarbu Lhakhang', 'Taktshang Lhakhang', 'Haa Valley', 'Kila Goemba', 'Chelela Pass', 'Dzongdrakha Goemba', 'Sangchen Choekhor Buddhist Institute'],
			'Thimphu'             => ['Memorial Chorten', 'Simtokha Dzong', 'National Library', 'Institute for Zorig Chusum', 'Traditional Medicine Institute', 'The Folk Heritage Museum', 'National Textile Museum', 'Trashichhoedzong', 'Changangkha Lhakhang', 'Zangthopelri Lhakhang', 'Buddha Point', 'Simply Bhutan Museum', 'Drubthob Goema / Zilukha Nunnery', 'Tango Goemba', 'Cheri Goemba', 'Phajoding Goemba', 'Lungchuzekha Goemba', 'Takin Preserve, Motithang', 'Botanical Gardens, Serbithang', 'Coronation Park', 'Tandin Nye', 'Sangaygang - Wangditse loop'],
			'Bumthang (Jakar)'    => ['Jambay Lhakhang', 'Kurje Lhakhang', 'Tamshing Lhakhang', 'Jakar Dzong', 'Konchogsum Lhakhang', 'Chankhar Lhakhang', 'Lhodrak Kharchhu Monastery', 'Tharpaling Monastery', 'Buli Lhakhang', 'Tangbi Goemba', 'Ngang Lhakhang', 'Ura Valley', 'Tang Valley', 'Membartsho (The Burning Lake)', 'Ugyenchholing Palace', 'Tang Rimochen Lhakhang', 'Kunzangdrak Goemba', 'Pelseling Goempa'],
			'Punakha'             => ['Punakha Dzong', 'Chimi Lhakhang', 'Khamsum Yulley Namgyal Chorten', 'Sangchhen Dorji Lhuendrup Lhakhang Nunnery', 'Limbukha', 'Talo', 'Punakha Ritsha Village', 'Nalanda Buddhist College'],
			'Mongar & Lhuntse'    => ['Mongar Dzong', 'Yakang Lhakhang', 'Drametse Lhakhang', 'Lhuntse'],
			'Wangduephodrang'     => ['Wangduephodrang Dzong'],
			'Trashigang'          => ['Trashigang Dzong', 'Gom Kora'],
		];

		foreach ($poi as $district => $places) {
			foreach ($places as $place) {
				\App\PointOfInterest::create([
					// 'uuid' => $faker->uuid,
					'name'     => $place,
					'district' => $district,
				]);
			}
		}
	}
}
