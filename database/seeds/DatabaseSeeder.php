<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {

		$this->call(VisitorsSeeder::class);
		$this->call(VisaSeeder::class);
		$this->call(PointOfInterestSeeder::class);
		$this->call(BorderSeeder::class);
		$this->call(VisitorActivitySeeder::class);
	}
}
