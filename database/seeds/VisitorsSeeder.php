<?php

use Illuminate\Database\Seeder;

class VisitorsSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();

		$countries = App\Country::pluck('id')->toArray();
		$marital_statuses = ['single', 'married', 'widowed', 'divorced'];
		$genders = ['M', 'F'];

		$current_count = 0;
		$split = \Carbon\Carbon::now();

		for ($i = 0; $i < env("VISITOR_COUNT", 100); $i++) {

			$current_count++;

			if ($current_count % 10000 == 0) {
				$secs = $split->diffInSeconds();
				$throughput = number_format(10000 / $secs, 2);
				$cc = number_format($current_count);
				$this->command->comment("  [$cc] -- Seed rate: $throughput/sec");
				$split = \Carbon\Carbon::now();
			}

			$gender = $genders[array_rand($genders)];
			$name = $faker->name($gender == 'M' ? 'male' : 'female');
			$data = [
				'uuid'				  => $faker->uuid,
				'name'                => $name,
				'gender'              => $gender,
				'passport_number'     => strtoupper($faker->bothify('?#######??')),
				'date_of_birth'       => $faker->dateTimeBetween($startDate = '-80 years', $endDate = '-10 years', $timezone = null),
				'country_of_birth'    => $countries[array_rand($countries)],
				'current_nationality' => $countries[array_rand($countries)],
				'marital_status'      => $marital_statuses[array_rand($marital_statuses)],
			];

			App\Visitor::create($data);

		}
	}
}
