<?php

use Illuminate\Database\Seeder;

class VisitorActivitySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();
		$pois = \App\PointOfInterest::pluck('id')->toArray();

		\App\Visitor::chunk(1000, function ($visitors) use ($faker, $pois) {
			$times = mt_rand(2, 11);
			for ($i = 0; $i < $times; $i++) {
				foreach ($visitors as $visitor) {
					$data = [
						'uuid'            => $faker->uuid,
						'poi_id'          => $faker->randomElement($pois),
						'passport_number' => $visitor->passport_number,
						'visit_date'      => $faker->date('Y-m-d', '-2 years'),
					];
					\App\VisitorActivity::create($data);
				}
			}
		});
	}
}
